import React, { FC, useContext, useEffect, useRef, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import '../index.css'



interface FormAddTableRow {
    onChangeDocumentNumber: Function
    onChangeAchivement: Function
    selectChange: Function
    onChangeScore: Function
    Open: Function
    onChangeMiracle: Function
    onChangeDate: Function

    onChangeLevelMiracle: Function
    onChangeStateMiracle: Function
    onChangeTypeMiracle: Function

    requestStatus: string
    achivement: string
    documentNumber: string
    score: number
    dateAchivement: string 
    typeMiracle: string | undefined
    levelMiracle: string | undefined
    stateMiracle: string | undefined
    documentTitle: string | undefined
    miracleAchivement: string | undefined
    linckDocs: string


    miracle: []
    achivementmainState: Array<{}>
    index: number
}


export const FormFieldTableRowAdmin: FC<FormAddTableRow> = (props: FormAddTableRow) => {
    //@ts-ignore
    const typeAchive = props.achivementmainState[props.index]?.typeAchivement
    //@ts-ignore
    const levelAchive = props.achivementmainState[props.index]?.levelAchivement
    //@ts-ignore
    const stateAchive = props.achivementmainState[props.index]?.stateAchivement

    return <tr>
        <td>
            <input
                onChange={e => props.onChangeAchivement(e)}
                placeholder={'Введите...'}
                value={props.achivement}
                type={'text'}
                
            />
        </td>
        <td>
            <select onChange={e => props.selectChange(e, props.index)} style={{ display: "block" }} >
                <option selected>{props.miracleAchivement ? props.miracleAchivement : '--Выберите--'}</option>
                {props.miracle.map(element => (

                    <option>{element['name']}</option>
                ))}
            </select>
        </td>
        <td>
            <select style={{ display: "block" }} onChange={e => props.onChangeTypeMiracle(e)} >
                <option selected>{props.typeMiracle ? props.typeMiracle : '--Выберите--'}</option>
                {typeAchive?.map((element: { [x: string]: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined }) => (
                    <option>{element['name']}</option>

                ))}
            </select>
        </td>
        <td>
            <select style={{ display: "block" }} onChange={e => props.onChangeLevelMiracle(e)} >
                <option selected>{props.levelMiracle ? props.levelMiracle : '--Выберите--'}</option>
                {levelAchive?.map((element: { [x: string]: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined }) => (
                    <option>{element['name']}</option>

                ))}

            </select>
        </td>
        <td>
            <select style={{ display: "block" }} onChange={e => props.onChangeStateMiracle(e)} >
                <option selected>{props.stateMiracle ? props.stateMiracle : '--Выберите--'}</option>
                {stateAchive?.map((element: { [x: string]: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined }) => (
                    <option>{element['name']}</option>

                ))}

            </select>
        </td>
        <td>
            <input
                onChange={e => props.onChangeDate(e)}
                value={props.dateAchivement}
                type='text'
                 />
        </td>
        <td>
            <input
                onChange={e => props.onChangeDocumentNumber(e)}
                placeholder={'При наличии'}
                value={props.documentNumber}
                type={'text'}
            />
        </td>
        <td>
                    <div className='row'>
                      <div className='col s2'>
                        <a
                          key={props.index}
                          className='waves-effect waves-light btn light-blue darken-1 tooltipped'

                          onClick={e=>props.Open(e)}
                          data-position='top' 
                          data-tooltip-img={props.linckDocs}
                        >
                          <i className='material-icons'>insert_drive_file</i>
                        </a>
                        <td><small className='small'>{props.linckDocs ? props.linckDocs.substring(props.linckDocs.lastIndexOf('/') + 1) : ""}</small></td>
                      </div>
                    </div>
        </td>

        <td>
            <input
                onChange={e => props.onChangeScore(e)}
                //@ts-ignore
                value={props.score}
                placeholder={'0'}
                type={'number'}
            />
        </td>

    </tr>

}