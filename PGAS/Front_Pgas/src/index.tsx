import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { CompanyProvider } from './store/CompanyContext'
import { RequestProvider } from './store/RequestContext'
import { RequestPaginateProvider } from './store/AdminRequestContext'

import { AuthProvider } from './store/AuthContext'
import App from './App'
import { NotificationProvider } from './store/NotificationContext'
import { AdminDetailContextProvider } from './store/AdminSearchContex'


ReactDOM.render(
  <NotificationProvider>
    <CompanyProvider>
      <RequestProvider>

        <RequestPaginateProvider>
          <AdminDetailContextProvider>
            <AuthProvider>
              <App />
            </AuthProvider>
          </AdminDetailContextProvider>
        </RequestPaginateProvider>

      </RequestProvider>
    </CompanyProvider>
  </NotificationProvider>,
  document.getElementById('root')
)
