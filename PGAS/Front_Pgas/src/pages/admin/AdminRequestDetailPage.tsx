import React, { FC, useContext, useEffect, useRef, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { AdminHeader } from '../../components/Header'
import { AdminRequestPaginateContext } from '../../store/AdminRequestContext'
import { RequestContext } from '../../store/RequestContext'
import M from 'materialize-css'
import { AuthContext } from '../../store/AuthContext'
import { useFormater } from '../../hooks/useFormater'
import $api from '../../http'
import { Link } from 'react-router-dom'
import { IRequestDetail } from '../../types/admindetailpage'
import { IDataInfoMiracle, IInfoMiracle, IRequest } from '../../types/request'
import { ISearchRequest } from '../../types/search'
import { AdminDetailContext } from '../../store/AdminSearchContex'
import {FormFieldTableRowAdmin} from '../../components/achivementTableRowAdmin'
import { request } from 'http'

export const AdminRequestDetailPage: FC = () => {
  const { id1 } = useParams()
  const messageRef = useRef(null)
  const btnRef = useRef(null)
  const [loading, setLoading] = useState(false)
  const { requests, nominations, statuses, addComment, setStatus, fetchDetailRequests } = useContext(AdminDetailContext)

  const { fio, avatarUrl, role, id } = useContext(AuthContext)

  const [message, setMessage] = useState('')
  const [achivementId, setAchivementId] = useState<number[]>([])
  const [achivementFileLink, setAchivementFileLink] = useState<string[]>([])
  const [achivementState, setAchivementState] = useState<Array<{
    typeAchivement: [],
    stateAchivement: [],
    levelAchivement: [],
    dictProgress: Number,
  }>>([])
  const _ = useFormater()
  const modalRef = useRef(null)
  const navigate = useNavigate()
  const [componentInfoAdmin, setComponentInfoAdmin] = useState<IInfoMiracle[]>([])
  const [changedAchivment, setChangedAchivment] = useState<string[]>([])

  //@ts-ignore
  const achivement = requests[0]?.nomination?.progress['progress']

  const selectChange = (event: React.ChangeEvent<HTMLSelectElement>, index: number) => {
    
    if(!changedAchivment.includes('Достижение')) changedAchivment.push('Достижение')
    const newArrayInfo = [...componentInfoAdmin]
    newArrayInfo[index].miracle = event.target.value
    newArrayInfo[index].typeMiracle = ''
    newArrayInfo[index].stateMiracle = ''
    newArrayInfo[index].levelMiracle = ''
    setComponentInfoAdmin(newArrayInfo)
    //@ts-ignore
    const testAchivement = requests[0]?.nomination?.progress['progress'].filter(r => r.name === event.target.value)
    const newArray = [...achivementState]
    if (newArray[index]) {
      newArray[index] = ({
        "dictProgress": testAchivement[0]?.id,
        //@ts-ignore
        "levelAchivement": requests[0]?.nomination.levelProgress['level'].filter(r => r['dictprogress'] === testAchivement[0]?.id),
        //@ts-ignore
        "stateAchivement": requests[0]?.nomination.statusProgress['statusProgress'].filter(r => r['dictprogress'] === testAchivement[0]?.id),
        //@ts-ignore
        "typeAchivement": requests[0]?.nomination.viewProgress['viewProgress'].filter(r => r['dictprogress'] === testAchivement[0]?.id),

      })
      setAchivementState(newArray)
    } else {
      newArray.push({
        "dictProgress": testAchivement[0]?.id,
        //@ts-ignore
        "levelAchivement": requests[0]?.nomination.levelProgress['level'].filter(r => r['dictprogress'] === testAchivement[0]?.id),
        //@ts-ignore
        "stateAchivement": requests[0]?.nomination.statusProgress['statusProgress'].filter(r => r['dictprogress'] === testAchivement[0]?.id),
        //@ts-ignore
        "typeAchivement": requests[0]?.nomination.viewProgress['viewProgress'].filter(r => r['dictprogress'] === testAchivement[0]?.id),

      })
      setAchivementState(newArray)
    }

  }

  const sendRequest = async () => {
    const resp = await $api.post('/api/requests/save/', {
      "componentInfo": componentInfoAdmin,
      "achivementId": achivementId,
      "achivementFileLink": achivementFileLink,
      "id": id1
    })
  }

  const sendScoreRequest = async (dataId: number | undefined, score: number) => {
    const resp = await $api.post('/api/requests/set-admin-row-point/', {
      "dataId": dataId,
      "score": score,
    })
  }

  useEffect(() => {
    if (id1 !== undefined) {
      fetchDetailRequests(parseInt(id1, 10))
    }

    
  }, [])


  useEffect(() => {
    if (requests[0]?.data?.data) {
      setComponentInfoAdmin(requests[0]?.data?.data)
    }
  }, [requests[0]?.data?.data])

  useEffect(() => {
    if (modalRef.current) {
      M.Modal.init(modalRef.current!)
    }


    M.FloatingActionButton.init(btnRef.current!, {
      toolbarEnabled: true,
    })
  }, [loading])


  useEffect(() => {
    document.querySelectorAll('.tooltipped').forEach(el => {
      const url = el.getAttribute('data-tooltip-img')
      M.Tooltip.init(el, {
        html: `<img src="${url}" class="tooltip-img" />`,
      })
    })
  })

  useEffect(() => {
    if (requests[0]?.data) {
      setAchivementFileLink([])
      setAchivementId([]) 
      componentInfoAdmin.map((element) => {

        //@ts-ignore
        const testAchivement = requests[0]?.nomination?.progress['progress'].filter(r => r.name === element.miracle)
        achivementState.push({
          "dictProgress": testAchivement[0]?.id,
          //@ts-ignore
          "levelAchivement": requests[0]?.nomination?.levelProgress['level'].filter(r => r['dictprogress'] === testAchivement[0]?.id),
          //@ts-ignore
          "stateAchivement": requests[0]?.nomination?.statusProgress['statusProgress'].filter(r => r['dictprogress'] === testAchivement[0]?.id),
          //@ts-ignore
          "typeAchivement": requests[0]?.nomination?.viewProgress['viewProgress'].filter(r => r['dictprogress'] === testAchivement[0]?.id),

        })
        
        setAchivementFileLink(oldArray => [...oldArray, element?.linckDocs])
        setAchivementId(oldIdArray => [...oldIdArray, element?.dataId])
       
      })

    }
  }, [componentInfoAdmin])

  // const saveHandler = async () => {
  //   try {
  //     M.toast({
  //       html: 'Данные были успешно сохранены!',
  //       classes: 'light-blue darken-1',
  //     })
  //   } catch (e) {
  //     // M.toast({
  //     //   html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
  //     //   classes: 'red darken-4',
  //     // })
  //   }
  // }
  const sendHandler = () => {
    try {
      if (message.trim().length === 0)
        return M.toast({
          html: `<span>Что-то пошло не так: <b>Комментрий не должен быть пустым!</b></span>`,
          classes: 'red darken-4',
        })

      addComment(requests[0].id!, fio, avatarUrl, message, role, id)
      // fetchDetailRequest() 
      setMessage('')
      M.toast({
        html: 'Вы успешно оставили комментарий!',
        classes: 'light-blue darken-1',
      })
    } catch (e) {
      // M.toast({
      //   html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
      //   classes: 'red darken-4',
      // })
    }
  }
  function formatDate(date: string | number | Date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    return [year, month, day].join('-');;
  }
  if (loading && !requests.length && !requests[0].data.data && !requests[0].data ) {
    return (
      <>
        <AdminHeader />
        <div className='my-center'>
          <div className='preloader-wrapper big active'>
            <div className='spinner-layer spinner-blue-only'>
              <div className='circle-clipper left'>
                <div className='circle'></div>
              </div>
              <div className='gap-patch'>
                <div className='circle'></div>
              </div>
              <div className='circle-clipper right'>
                <div className='circle'></div>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }

  return (
    <>
      <AdminHeader />
      <div style={{ alignSelf: "center" }} className='container'>
        <h3 className='mt-4'>Информация о заявлении</h3>
        <table className='responsive-table'>
          <thead className='striped'>
            <tr>
              <th>Кампания</th>
              <th>Критерий</th>
              <th>Статус</th>
              <th>Дата создания</th>
              <th>Дата последнего изменения</th>
            </tr>
          </thead>

          {requests?.map((r) => {
            return (
              <tbody>
                <tr>
                  <td>{r?.company?.name}</td>
                  <td>{r?.nomination?.name}</td>
                  <td>{r?.status}</td>
                  <td>{formatDate(r?.createdDate)}</td>
                  <td>{formatDate(r?.changedDate)}</td>
                </tr>
              </tbody>

            )
          })}
        </table>
        <h3 className='mt-4'>Информация о студенте</h3>
        {/* <Link to={`/edit-application/${requestDetail?.id}`}><button className='btn light-blue darken-2'>Редактировать</button></Link> */}
        <table className='responsive-table'>
          <thead>
            <tr>
              <th>ФИО</th>
              <th>Телефон</th>
              <th>Институт</th>
              <th>Направление</th>
              <th>Форма обучения</th>
              <th>Источник финансирования</th>
              <th>Уровень образования</th>
              <th>Курс</th>
            </tr>
          </thead>
          {requests?.map(r => {
            return (
              <tbody>
                <tr>
                  <td>{r?.student?.fio}</td>
                  <td>{r?.student?.phone}</td>
                  <td>{r?.student?.institute}</td>
                  <td>{r?.student?.direction}</td>
                  <td>{r?.student?.educationForm}</td>
                  <td>{r?.student?.financingSource}</td>
                  <td>{r?.student?.level}</td>
                  <td>{r?.student?.course}</td>
                </tr>
              </tbody>
            )
          })}

        </table>

        <h3 className='mt-4'>Достижения</h3>
        <table className='responsive-table table-width com-4'>
          <thead>
            <tr>
              <th>Наименование достижения</th>
              <th style={{ width: "12%" }}>Достижение</th>
              <th>Вид достижения</th>
              <th>Уровень достиженя</th>
              <th>Статус достижения</th>
              <th>Дата мероприятия (Пример: 2000-01-22 (гггг-мм-дд))</th>
              <th style={{ width: "10%" }}>Номер документа</th>
              <th style={{ width: "15%" }}>Документ</th>
              <th style={{ width: "2%" }}>Баллы</th>
            </tr>
          </thead>
          {requests[0]?.status === 'Отправлено на рассмотрение' || requests[0]?.status === 'Отправлено на доработку' ?
                componentInfoAdmin.map((element, index) => {
                  return (
                    <FormFieldTableRowAdmin
                      achivement={element.achivement}
                      documentNumber={element.documentNumber}
                      score={element.score}
                      dateAchivement={element.dateAchivement}
                      miracle={achivement}
                      miracleAchivement={element.miracle}
                      achivementmainState={achivementState}
                      selectChange={selectChange}
                      index={index}
                      typeMiracle={element.typeMiracle}
                      levelMiracle={element.levelMiracle}
                      stateMiracle={element.stateMiracle}
                      documentTitle={element.documentTitle}
                      requestStatus={requests[0]?.status}
                      linckDocs={element.linckDocs}

                      onChangeAchivement={(e: { target: { value: any } }) => {
                        const newArray = [...componentInfoAdmin]
                        newArray[index].achivement = e.target.value
                        if(!changedAchivment.includes('Наименование достижения')) changedAchivment.push('Наименование достижения')
                        setComponentInfoAdmin(newArray)
                        
                      }}

                      onChangeMiracle={(e: { target: { value: any } }) => {
                        
                        const newArray = [...componentInfoAdmin]
                        newArray[index].miracle = e.target.value
                        
                        
                        setComponentInfoAdmin(newArray)
                      }}

                      onChangeTypeMiracle={(e: { target: { value: any } }) => {
                        const newArray = [...componentInfoAdmin]
                        newArray[index].typeMiracle = e.target.value
                        if(!changedAchivment.includes('Вид достижения')) changedAchivment.push('Вид достижения')
                        setComponentInfoAdmin(newArray)
                      }}

                      onChangeLevelMiracle={(e: { target: { value: any } }) => {
                        const newArray = [...componentInfoAdmin]
                        newArray[index].levelMiracle = e.target.value
                        if(!changedAchivment.includes('Уровень достижения')) changedAchivment.push('Уровень достижения')
                        setComponentInfoAdmin(newArray)
                      }}

                      onChangeStateMiracle={(e: { target: { value: any } }) => {
                        const newArray = [...componentInfoAdmin]
                        newArray[index].stateMiracle = e.target.value
                        if(!changedAchivment.includes('Статус достижения')) changedAchivment.push('Статус достижения')
                        setComponentInfoAdmin(newArray)
                      }}

                      onChangeScore={(e: { target: { value: any } }) => {
                        const newArray = [...componentInfoAdmin]
                        newArray[index].score = e.target.value
                        setComponentInfoAdmin(newArray)
                        sendScoreRequest(element.dataId,e.target.value)
                      }}
                      onChangeDate={(e: { target: { value: any } }) => {
                        const newArray = [...componentInfoAdmin]
                        newArray[index].dateAchivement = e.target.value
                        if(!changedAchivment.includes('Дата мероприятия')) changedAchivment.push('Дата мероприятия')
                        setComponentInfoAdmin(newArray)
                      }}
                      onChangeDocumentNumber={(e: { target: { value: any } }) => {
                        const newArray = [...componentInfoAdmin]
                        newArray[index].documentNumber = e.target.value
                        if(!changedAchivment.includes('Номер документа')) changedAchivment.push('Номер документа')
                        setComponentInfoAdmin(newArray)
                      }}
                      Open = {(e:{target:{value:any}})=>{
                        const newArray = [...componentInfoAdmin]
                        window.open(newArray[index].linckDocs, '_blank')

                      }}
                     

                    />)
                    
                    
                }
                )
                
          
          
          : componentInfoAdmin.map((r, index) => {
            return (
              <tbody>
                <tr>
                  <td>{r.achivement}</td>
                  <td>{r.miracle}</td>
                  <td>{r.typeMiracle}</td>
                  <td>{r.levelMiracle}</td>
                  <td>{r.stateMiracle}</td>
                  <td>{r.dateAchivement}</td>
                  <td>{r.documentNumber}</td>
                  <td>
                    <div className='row'>
                      <div className='col s2'>
                        <a
                          key={r.dataId}
                          className='waves-effect waves-light btn light-blue darken-1 tooltipped'
                          target='_blank'
                          href={r.linckDocs}
                          data-position='top'
                          data-tooltip-img={r.linckDocs}
                        >
                          <i className='material-icons'>insert_drive_file</i>
                        </a>
                        <td><small className='small'>{r.linckDocs ? r.linckDocs.substring(r.linckDocs.lastIndexOf('/') + 1) : ""}</small></td>
                      </div>
                    </div>
                  </td>
                  <td>

                  <input
                      onChange={(e: { target: { value: any } }) => {
                        
                        const newArray = [...componentInfoAdmin]
                        newArray[index].score = e.target.value
                        setComponentInfoAdmin(newArray)
                        sendScoreRequest(r.dataId, e.target.value)
                      }}
                      value={r?.score}
                      id={"point" + index}
                      placeholder={'0'}
                      type={'number'}
                    />
                  </td>
                </tr>
              </tbody>
            )

          })}

          
        </table>

        <button
          className='btn light-blue darken-2 waves-effect waves-light center-btn com-4'
          style={{ float: "right", marginTop: "0.9em", marginRight: "0.0em" }}
          onClick={() => {
            sendRequest()
          }}
        >
          Сохранить изменения
        </button>
        {/* <button
          className='btn light-blue darken-2 waves-effect waves-light'
          style={{ marginTop: 36, float: 'right' }}
          onClick={saveHandler}
        >
          <i className='material-icons left'>save</i>
          Сохранить изменения
        </button> */}
        <h3 className='mt-4'>Комментарии</h3>
        <div>
          {requests[0]?.comments?.map((c, idx) => {
            return (
              <div key={idx} className='comment'>
                <div className='avatar'>
                  {/* <img src={c.imageUrl} alt='avatar' /> */}
                  <span>{c.name}</span>
                </div>
                <p>{c.text}</p>
                <small>{_(c.sendedDate)}</small>
              </div>
            )
          })}
        </div>
        <div className='input-field'>
          <textarea
            id='message'
            className='materialize-textarea mt-4'
            data-length='1000'
            value={message}
            onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
              if (message.length <= 1000) setMessage(event.target.value)
            }}
            ref={messageRef}
          ></textarea>
          <label className='message'>Сообщение</label>
        </div>
        <button
          className='btn light-blue darken-2 waves-effect waves-light'
          style={{ float: 'right' }}
          onClick={sendHandler}
        >
          <i className='material-icons left'>send</i>
          Отправить комментарий
        </button>
      </div>
      {!(
        // subRequest?.status === 'Победитель' ||
        // subRequest?.status === 'Принято' ||
        (requests[0]?.status === 'Удалено')
      ) ? (
        <div className='fixed-action-btn toolbar' ref={btnRef}>
          <a className='btn-floating btn-large light-blue darken-4 pulse'>
            <i className='large material-icons'>mode_edit</i>
          </a>
          <ul>
            <li>
              <a>
                <button
                  className='waves-effect waves-light yellow darken-2 btn'
                  onClick={() => {
                    try {
                      setStatus(Number(id1), 'Отправлено на доработку');
                      console.log(changedAchivment)
                      if(changedAchivment.length)
                      {
                      addComment(
                        Number(id1),
                        fio,
                        avatarUrl,
                        `Статус изменён на "Отправлено на доработку". Администратор изменил следующие поля: ${changedAchivment.map(elem=>`"${elem}"`)}`,
                        role,
                        Number(id)
                        
                      );
                    }else{
                      addComment(
                        Number(id1),
                        fio,
                        avatarUrl,
                        'Статус изменён на "Отправлено на доработку"',
                        role,
                        Number(id)
                        
                      );

                    }

                      const i = M.Modal.getInstance(modalRef.current!)
                      i.open()

                      M.toast({
                        html: '<span>Вы успешно выставили статус <strong>Отправлено на доработку</strong> !</span>',
                        classes: 'light-blue darken-1',
                      });

                    } catch (e) {
                      M.toast({
                        html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
                        classes: 'red darken-4',
                      })
                    }
                  }}
                >
                  Отправлено на доработку
                </button>
              </a>
            </li>
            <li>
              <a>
                <button
                  className='waves-effect waves-light light-blue darken-3 btn'
                  onClick={() => {
                    try {
                      setStatus(Number(id1), 'Принято')
                      addComment(
                        Number(id1),
                        fio,
                        avatarUrl,
                        'Статус изменён на "Принято"',
                        role,
                        Number(id)
                      )
                      M.toast({
                        html: '<span>Вы успешно выставили статус <strong>Принято</strong> !</span>',
                        classes: 'light-blue darken-1',
                      });
                      // fetchDetailRequest();
                    } catch (e) {
                      M.toast({
                        html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
                        classes: 'red darken-4',
                      })
                    }
                  }}
                >
                  Принято
                </button>
              </a>
            </li>
            <li>
              <a>
                <button
                  className='waves-effect waves-light teal darken-1 btn'
                  onClick={() => {
                    try {
                      setStatus(Number(id1), 'Победитель')
                      addComment(
                        Number(id1),
                        fio,
                        avatarUrl,
                        'Статус изменён на "Победитель"',
                        role,
                        Number(id)
                      )
                      M.toast({
                        html: '<span>Вы успешно выставили статус <strong>Победитель</strong> !</span>',
                        classes: 'light-blue darken-1',
                      });
                      // fetchDetailRequest();
                    } catch (e) {
                      M.toast({
                        html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
                        classes: 'red darken-4',
                      })
                    }
                  }}
                >
                  Победитель
                </button>
              </a>
            </li>
            <li>
              <a>
                <button
                  className='waves-effect waves-light red darken-1 btn'
                  onClick={() => {
                    try {
                      setStatus(
                        Number(id1),
                        'Отказать по решению Стипендиальной Комиссии'
                      )
                      addComment(
                        Number(id1),
                        fio,
                        avatarUrl,
                        'Статус изменён на "Отказать по решению Стипендиальной Комиссии"',
                        role,
                        Number(id)
                      )

                      M.toast({
                        html: '<span>Вы успешно выставили статус <strong>Отказать по решению Стипендиальной Комиссии</strong> !</span>',
                        classes: 'light-blue darken-1',
                      });
                      // fetchDetailRequest();

                    } catch (e) {
                      M.toast({
                        html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
                        classes: 'red darken-4',
                      })
                    }
                  }}
                >
                  Отказать по решению Стипендиальной Комиссии
                </button>
              </a>
            </li>
          </ul>
        </div>
      ) : null}
      <div style={{ height: 100 }}></div>

      {/* modal */}
      <div ref={modalRef} className='modal'>
        <div className='modal-content'>
          <h4>Оставьте комментарии</h4>
          <div style={{ height: 20 }} />
          <div className='input-field'>
            <textarea
              id='message'
              className='materialize-textarea mt-4'
              data-length='1000'
              value={message}
              onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
                if (message.length <= 1000) setMessage(event.target.value)
              }}
              ref={messageRef}
            ></textarea>
            <label className='message'>Сообщение</label>
          </div>
          <button
            className='btn light-blue darken-2 waves-effect waves-light'
            style={{ float: 'right' }}
            onClick={() => {
              sendHandler()

              //navigate('/admin/requests/')
            }}
          >
            <i className='material-icons left'>send</i>
            Отправить комментарий
          </button>
          <div style={{ height: 20 }} />
        </div>
      </div>
    </>
  )
}
