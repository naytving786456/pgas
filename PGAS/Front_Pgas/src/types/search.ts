import { Type } from 'typescript'
import { Role } from './auth'
import { ICompany } from './companies'
import { IComment, IDataInfoMiracle } from './request'
import { INomination } from './admindetailpage'
 
export interface ISearchRequest {
    id: number
    last_status: string
    company: {
      name: string
      id: number
      endDate: Date
      startDate: Date
    
    }
    student: {
      id: number
      firstname: string
      lastname: string| undefined
      patronymic: string| undefined
      institut: string | undefined
      profile: string | undefined
      form: string | undefined
      phone: string | undefined
      financingSource: string | undefined
      level: string | undefined
      course: string | number | undefined
    }
    typeMiracle: INomination
    data: IDataInfoMiracle
    CreatedOn: string
    changedDate: Date
    comments: IComment[]
  }



